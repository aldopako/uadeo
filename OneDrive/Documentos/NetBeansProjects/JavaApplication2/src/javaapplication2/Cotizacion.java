/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author aldom
 */
public class Cotizacion {
    private int numCotizacion;
    private float ValorAuto;
    private float PorcentajePagoInicial;
    private String Descripcion;
    private int Plazos;
    
    
    
    
    
public Cotizacion(){
    
    
this.numCotizacion=0;
this.ValorAuto=0.0f;
this.PorcentajePagoInicial=0.0f;
this.Descripcion="";
this.Plazos=0;
}


public  Cotizacion(int Plazos, float ValorAuto, float ProcentajePagoInicial, int numCotizacion, String Descripcion){
    
    this.Descripcion = Descripcion;
    this.Plazos = Plazos;
    this.PorcentajePagoInicial = PorcentajePagoInicial;
    this.ValorAuto = ValorAuto;
    this.numCotizacion = numCotizacion;
    
    }

 public Cotizacion(Cotizacion otro){
    this.Descripcion = otro.Descripcion;
    this.Plazos = otro.Plazos;
    this.PorcentajePagoInicial = otro.PorcentajePagoInicial;
    this.ValorAuto = otro.ValorAuto;
    this.numCotizacion = otro.numCotizacion;
    
    
    
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public float getValorAuto() {
        return ValorAuto;
    }

    public void setValorAuto(float ValorAuto) {
        this.ValorAuto = ValorAuto;
    }

    public float getPorcentajePagoInicial() {
        return PorcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float PorcentajePagoInicial) {
        this.PorcentajePagoInicial = PorcentajePagoInicial;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getPlazos() {
        return Plazos;
    }

    public void setPlazos(int Plazos) {
        this.Plazos = Plazos;
    }
 
    //metodos de comportamiento
    public float CalculoPagoInicial(){
    float Pagoinicial = 0.0f;
    Pagoinicial = (this.ValorAuto)*0.25f;
    return Pagoinicial;
    
    }
    
    

}
