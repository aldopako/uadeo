package javaapplication3;

/**
 *
 * @author aldom
 */
public class producto {
    
    private int codigoP;
    private String descripcion;
    private String unidadM;
    private float precioC;
    private float precioV;
    private int cantidadP;
    public float ganancia;
    public float precioVT;
    public float precioCT;
 
    
    public producto(){
    this.codigoP=0;
    this.descripcion="";
    this.unidadM="";
    this.precioC=0.0f;
    this.precioV=0.0f;
    this.cantidadP=0;
    this.ganancia=0.0f;
    this.precioVT=0.0f;
    this.precioCT=0.0f;
    
}
    
    public producto(int codigo,String descipcion,String unidadM,float precioC,float precioV,int cantidadP,float calcularPVT,float calcularPCT,float ganancia,float precioVT,float precioCT){
    
        this.codigoP=codigoP;
    this.descripcion=descripcion;
    this.unidadM=unidadM;
    this.precioC=precioC;
    this.precioV=precioV;
    this.cantidadP=cantidadP;
    this.ganancia=ganancia;
    this.precioVT=precioVT;
    this.precioCT=precioCT;
    
    
    }
    

    public int getCodigoP() {
        return codigoP;
    }

    public void setCodigoP(int codigoP) {
        this.codigoP = codigoP;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadM() {
        return unidadM;
    }

    public void setUnidadM(String unidadM) {
        this.unidadM = unidadM;
    }

    public float getPrecioC() {
        return precioC;
    }

    public void setPrecioC(float precioC) {
        this.precioC = precioC;
    }

    public float getPrecioV() {
        return precioV;
    }

    public void setPrecioV(float precioV) {
        this.precioV = precioV;
    }

    public int getCantidadP() {
        return cantidadP;
    }

    public void setCantidadP(int cantidadP) {
        this.cantidadP = cantidadP;
    }
    
    //calcular metodos
    
    public float calcularPVT(){
        
        float calcularPVT = precioV*cantidadP;
        
        return calcularPVT;
    
}
      public float calcularPCT(){
        
        float calcularPCT = precioC*cantidadP;
        
        return calcularPCT;
    
}
      
      public float ganancia(){
          
          precioCT=precioC*cantidadP;
          precioVT=precioV*cantidadP;
          ganancia=precioVT-precioCT;
        return ganancia;
          
      }
    
    
}